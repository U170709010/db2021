-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema us_balance_sheet
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema us_balance_sheet
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `us_balance_sheet` DEFAULT CHARACTER SET utf8 ;
USE `us_balance_sheet` ;

-- -----------------------------------------------------
-- Table `ShareholdersEquity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShareholdersEquity` ;

CREATE TABLE IF NOT EXISTS `ShareholdersEquity` (
  `idShareholdersEquity` INT NOT NULL AUTO_INCREMENT,
  `ShareCapital_APIC` BIGINT NULL,
  `Treasury_Stock` BIGINT NULL,
  `Retained_Earnings` BIGINT NULL,
  PRIMARY KEY (`idShareholdersEquity`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `companystock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `companystock` ;

CREATE TABLE IF NOT EXISTS `companystock` (
  `idcompanystock` INT NOT NULL AUTO_INCREMENT,
  `Ticker` VARCHAR(155) NOT NULL,
  `SimFinId` INT NOT NULL,
  `Shares_Basic` BIGINT NULL,
  `Shares_Diluted` BIGINT NULL,
  `ShareholdersEquity_idShareholdersEquity` INT NOT NULL,
  PRIMARY KEY (`idcompanystock`),
  CONSTRAINT `fk_companystock_ShareholdersEquity1`
    FOREIGN KEY (`ShareholdersEquity_idShareholdersEquity`)
    REFERENCES `ShareholdersEquity` (`idShareholdersEquity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_companystock_ShareholdersEquity1_idx` ON `companystock` (`ShareholdersEquity_idShareholdersEquity` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `financialreport`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `financialreport` ;

CREATE TABLE IF NOT EXISTS `financialreport` (
  `idfinancialreport` INT NOT NULL AUTO_INCREMENT,
  `FiscalYear` YEAR NOT NULL,
  `ReportDate` DATE NOT NULL,
  `PublishDate` DATE NOT NULL,
  `RestatedDate` DATE NOT NULL,
  `companystock_idcompanystock` INT NOT NULL,
  PRIMARY KEY (`idfinancialreport`, `companystock_idcompanystock`),
  CONSTRAINT `fk_financialreport_companystock`
    FOREIGN KEY (`companystock_idcompanystock`)
    REFERENCES `companystock` (`idcompanystock`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_financialreport_companystock_idx` ON `financialreport` (`companystock_idcompanystock` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `CurrentAsset`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CurrentAsset` ;

CREATE TABLE IF NOT EXISTS `CurrentAsset` (
  `idCurrentAsset` INT NOT NULL AUTO_INCREMENT,
  `Cash_Cash_Equivalents_Short_Term_Investments` BIGINT NULL,
  `Accounts_Notes_Receivable` BIGINT NULL,
  `Inventories` BIGINT NULL,
  `Total_Current_Assets` BIGINT NULL,
  `companystock_idcompanystock` INT NOT NULL,
  PRIMARY KEY (`idCurrentAsset`),
  CONSTRAINT `fk_CurrentAsset_companystock1`
    FOREIGN KEY (`companystock_idcompanystock`)
    REFERENCES `companystock` (`idcompanystock`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_CurrentAsset_companystock1_idx` ON `CurrentAsset` (`companystock_idcompanystock` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `LongTermAssets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LongTermAssets` ;

CREATE TABLE IF NOT EXISTS `LongTermAssets` (
  `idLongTermAssets` INT NOT NULL AUTO_INCREMENT,
  `Property_Plant_Equipment_Net` BIGINT NULL,
  `Long_Term_Investments_Receivables` BIGINT NULL,
  `Other_Long_Term_Assets` BIGINT NULL,
  `Total_Noncurrent_Assets` BIGINT NULL,
  `companystock_idcompanystock` INT NOT NULL,
  PRIMARY KEY (`idLongTermAssets`),
  CONSTRAINT `fk_LongTermAssets_companystock1`
    FOREIGN KEY (`companystock_idcompanystock`)
    REFERENCES `companystock` (`idcompanystock`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_LongTermAssets_companystock1_idx` ON `LongTermAssets` (`companystock_idcompanystock` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `TotalAssets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TotalAssets` ;

CREATE TABLE IF NOT EXISTS `TotalAssets` (
  `Total_Assets` BIGINT NOT NULL,
  `CurrentAsset_idCurrentAsset` INT NOT NULL,
  `LongTermAssets_idLongTermAssets` INT NOT NULL,
  PRIMARY KEY (`CurrentAsset_idCurrentAsset`, `LongTermAssets_idLongTermAssets`),
  CONSTRAINT `fk_TotalAssets_CurrentAsset1`
    FOREIGN KEY (`CurrentAsset_idCurrentAsset`)
    REFERENCES `CurrentAsset` (`idCurrentAsset`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TotalAssets_LongTermAssets1`
    FOREIGN KEY (`LongTermAssets_idLongTermAssets`)
    REFERENCES `LongTermAssets` (`idLongTermAssets`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_TotalAssets_LongTermAssets1_idx` ON `TotalAssets` (`LongTermAssets_idLongTermAssets` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `LongTermDebt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LongTermDebt` ;

CREATE TABLE IF NOT EXISTS `LongTermDebt` (
  `idLongTermDebt` INT NOT NULL AUTO_INCREMENT,
  `Long_Term_Debt` BIGINT NULL,
  `Total_Noncurrent_Liabilities` BIGINT NULL,
  `companystock_idcompanystock` INT NOT NULL,
  PRIMARY KEY (`idLongTermDebt`),
  CONSTRAINT `fk_LongTermDebt_companystock1`
    FOREIGN KEY (`companystock_idcompanystock`)
    REFERENCES `companystock` (`idcompanystock`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_LongTermDebt_companystock1_idx` ON `LongTermDebt` (`companystock_idcompanystock` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `CurrentLiabilities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CurrentLiabilities` ;

CREATE TABLE IF NOT EXISTS `CurrentLiabilities` (
  `idCurrentLiabilities` INT NOT NULL AUTO_INCREMENT,
  `Payables_Accruals` BIGINT NULL,
  `Short_Term_Debt` BIGINT NULL,
  `Total_Current_Liabilities` BIGINT NULL,
  `companystock_idcompanystock` INT NOT NULL,
  PRIMARY KEY (`idCurrentLiabilities`),
  CONSTRAINT `fk_CurrentLiabilities_companystock1`
    FOREIGN KEY (`companystock_idcompanystock`)
    REFERENCES `companystock` (`idcompanystock`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_CurrentLiabilities_companystock1_idx` ON `CurrentLiabilities` (`companystock_idcompanystock` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `TotalLiabilities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TotalLiabilities` ;

CREATE TABLE IF NOT EXISTS `TotalLiabilities` (
  `Total_Liabilities` BIGINT NOT NULL,
  `LongTermDebt_idLongTermDebt` INT NOT NULL,
  `CurrentLiabilities_idCurrentLiabilities` INT NOT NULL,
  PRIMARY KEY (`LongTermDebt_idLongTermDebt`, `CurrentLiabilities_idCurrentLiabilities`),
  CONSTRAINT `fk_TotalLiabilities_LongTermDebt1`
    FOREIGN KEY (`LongTermDebt_idLongTermDebt`)
    REFERENCES `LongTermDebt` (`idLongTermDebt`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TotalLiabilities_CurrentLiabilities1`
    FOREIGN KEY (`CurrentLiabilities_idCurrentLiabilities`)
    REFERENCES `CurrentLiabilities` (`idCurrentLiabilities`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_TotalLiabilities_CurrentLiabilities1_idx` ON `TotalLiabilities` (`CurrentLiabilities_idCurrentLiabilities` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `TotalEquity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TotalEquity` ;

CREATE TABLE IF NOT EXISTS `TotalEquity` (
  `Total_Equity` BIGINT NULL,
  `Total_Liabilities_Equity` BIGINT NOT NULL,
  `TotalAssets_CurrentAsset_idCurrentAsset` INT NOT NULL,
  `TotalAssets_LongTermAssets_idLongTermAssets` INT NOT NULL,
  `TotalLiabilities_LongTermDebt_idLongTermDebt` INT NOT NULL,
  `TotalLiabilities_CurrentLiabilities_idCurrentLiabilities` INT NOT NULL,
  PRIMARY KEY (`TotalAssets_CurrentAsset_idCurrentAsset`, `TotalAssets_LongTermAssets_idLongTermAssets`, `TotalLiabilities_LongTermDebt_idLongTermDebt`, `TotalLiabilities_CurrentLiabilities_idCurrentLiabilities`),
  CONSTRAINT `fk_TotalEquity_TotalAssets1`
    FOREIGN KEY (`TotalAssets_CurrentAsset_idCurrentAsset` , `TotalAssets_LongTermAssets_idLongTermAssets`)
    REFERENCES `TotalAssets` (`CurrentAsset_idCurrentAsset` , `LongTermAssets_idLongTermAssets`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TotalEquity_TotalLiabilities1`
    FOREIGN KEY (`TotalLiabilities_LongTermDebt_idLongTermDebt` , `TotalLiabilities_CurrentLiabilities_idCurrentLiabilities`)
    REFERENCES `TotalLiabilities` (`LongTermDebt_idLongTermDebt` , `CurrentLiabilities_idCurrentLiabilities`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_TotalEquity_TotalLiabilities1_idx` ON `TotalEquity` (`TotalLiabilities_LongTermDebt_idLongTermDebt` ASC, `TotalLiabilities_CurrentLiabilities_idCurrentLiabilities` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `denormalized`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `denormalized` ;

CREATE TABLE IF NOT EXISTS `denormalized` (
  `Ticker` VARCHAR(155) NULL,
  `SimFinId` INT NULL,
  `FiscalYear` YEAR NULL,
  `ReportDate` DATE NULL,
  `PublishDate` DATE NULL,
  `RestatedDate` DATE NULL,
  `Shares_Basic` BIGINT NULL,
  `Shares_Diluted` BIGINT NULL,
  `Cash_Cash_Equivalents_Short_Term_Investments` BIGINT NULL,
  `Accounts_Notes_Receivable` BIGINT NULL,
  `Inventories` BIGINT NULL,
  `Total_Current_Assets` BIGINT NULL,
  `Property_Plant_Equipment_Net` BIGINT NULL,
  `Long_Term_Investments_Receivables` BIGINT NULL,
  `Other_Long_Term_Assets` BIGINT NULL,
  `Total_Nuncurrent_Assets` BIGINT NULL,
  `Total_Assets` BIGINT NULL,
  `Payables_Accruals` BIGINT NULL,
  `Short_Term_Debt` BIGINT NULL,
  `Total_Current_Liabilites` BIGINT NULL,
  `Long_Term_Debt` BIGINT NULL,
  `Total_Noncurrent_Liabilities` BIGINT NULL,
  `Total_Liabilities` BIGINT NULL,
  `Share_Capital_APIC` BIGINT NULL,
  `Treasury_Stock` BIGINT NULL,
  `Retained_Earnings` BIGINT NULL,
  `Total_Equity` BIGINT NULL,
  `Total_Liabilities_Equity` BIGINT NULL)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ShareholdersEquity`
-- -----------------------------------------------------
START TRANSACTION;
USE `us_balance_sheet`;
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (1, 7416000000, -7470000000, 2791000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (2, 7558000000, -7627000000, 2760000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (3, 7910000000, -8038000000, 3444000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (4, 8271000000, -8535000000, 4456000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (5, 8495000000, -8707000000, 5505000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (6, 8729000000, -9607000000, 6073000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (7, 8973000000, -9807000000, 6469000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (8, 9051000000, -10074000000, 5581000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (9, 9165000000, -10508000000, 6089000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (10, 5303000000, NULL, -126000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (11, 5311000000, NULL, -336000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (12, 5280000000, NULL, -18000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (13, 0, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (14, 9533000000, NULL, -104000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (15, 9592000000, NULL, 113000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (16, 9613000000, NULL, 341000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (17, 9641000000, NULL, -555000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (18, 145987000, NULL, 19119000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (19, 4738000000, -367000000, -5136000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (20, 4784000000, -367000000, -5607000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (21, 4806000000, -367000000, -7586000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (22, 4822000000, -367000000, -9462000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (23, 10597000000, NULL, -11296000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (24, 15142000000, NULL, -8562000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (25, 11597000000, NULL, -1230000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (26, 7228000000, NULL, 1640000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (27, 5719000000, NULL, -1345000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (28, 4969000000, NULL, 136000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (29, 3949000000, NULL, 2264000000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (30, 12879000, 0, -5339000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (31, 14177000, -245468000, 54174000);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (32, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (33, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (34, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (35, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (36, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (37, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (38, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (39, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (40, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (41, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (42, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (43, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (44, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (45, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (46, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (47, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (48, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (49, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (50, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (51, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (52, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (53, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (54, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (55, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (56, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (57, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (58, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (59, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (60, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (61, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (62, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (63, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (64, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (65, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (66, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (67, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (68, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (69, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (70, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (71, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (72, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (73, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (74, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (75, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (76, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (77, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (78, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (79, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (80, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (81, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (82, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (83, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (84, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (85, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (86, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (87, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (88, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (89, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (90, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (91, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (92, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (93, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (94, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (95, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (96, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (97, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (98, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (99, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (100, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (101, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (102, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (103, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (104, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (105, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (106, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (107, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (108, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (109, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (110, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (111, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (112, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (113, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (114, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (115, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (116, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (117, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (118, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (119, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (120, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (121, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (122, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (123, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (124, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (125, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (126, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (127, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (128, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (129, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (130, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (131, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (132, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (133, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (134, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (135, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (136, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (137, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (138, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (139, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (140, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (141, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (142, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (143, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (144, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (145, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (146, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (147, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (148, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (149, NULL, NULL, NULL);
INSERT INTO `ShareholdersEquity` (`idShareholdersEquity`, `ShareCapital_APIC`, `Treasury_Stock`, `Retained_Earnings`) VALUES (150, NULL, NULL, NULL);

COMMIT;

